import "dart:math" show pi;
import "dart:math" show cos;
import "dart:math" show sin;
import "dart:math" show atan;
import "dart:math" show sqrt;


void main() {
  // print(GeoMath.toGMS(37.15));
}

class GeoMath {
  static num _toRad([double deg = 180.0]) {
    num rad = deg * pi / 180;
    return rad;
  }

  static double toDeg([double degree = 0, double min = 0, double sec = 0]) {
    double deg = degree + min / 60 + sec / 3600;
    return deg;
  }

  static String toGMS([double deg = 0]) {
    int degree = deg.toInt();
    double min = ((deg - deg.toInt()) * 60);
    double sec = double.parse(((min - min.toInt()) * 60).toStringAsFixed(2));
    return "${degree.toString()}° ${min.toInt().toString()}′ ${sec.toString()}′′";
  }
  static String DirectTask([double X = 0, double Y = 0, double d = 0, double degree = 0, double min = 0, double sec = 0]) {
    double ugol = toDeg(degree, min, sec);
    double delX = d*cos(_toRad(ugol));
    double delY = d*sin(_toRad(ugol));
    double Xb = 0.0;
    double Yb = 0.0;
    if (ugol >= 90 && ugol < 180){
      ugol = 180 - ugol;
    }
    if (ugol >= 180 && ugol < 270){
      ugol = ugol - 180;
    }
    if (ugol >= 270 && ugol < 360){
      ugol = 360 - ugol;
    }
    Xb = X + delX;
    Yb = Y + delY;
    return "ugol = ${ugol.toString()} delX = ${delX.toString()} delY = ${delY.toString()} \n Xb = ${Xb.toString()}, Yb = ${Yb.toString()}";
  }
  static String RoundTask([double Xa = 0, double Ya = 0, double Xb = 0, double Yb = 0,] ){
    double delX = Xb - Xa;
    double delY = Yb - Ya;
    double rumb = atan(_toRad((delY/delX).abs()));
    if (delX < 0 && delY > 0) rumb = 180 - rumb;
    if (delX < 0 && delY < 0) rumb = rumb - 180;
    if (delX > 0 && delY < 0) rumb = 360 - rumb;
    double d = sqrt(delX*delX + delY*delY);
    return "delX = ${delX.toString()} delY = ${delY.toString()} \n  d = ${d.toString()}";
  }
}