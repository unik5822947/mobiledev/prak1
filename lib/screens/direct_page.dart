import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prak1/utils/geo_math.dart';

void main() => runApp(const DirectPage());

class DirectPage extends StatefulWidget {
  const DirectPage({super.key});

  @override
  State<DirectPage> createState() => _nameState();
}

class _nameState extends State<DirectPage> {
  final TextEditingController _Xa = TextEditingController();
  final TextEditingController _Ya = TextEditingController();
  final TextEditingController _d = TextEditingController();
  final TextEditingController _degree = TextEditingController();
  final TextEditingController _mins = TextEditingController();
  final TextEditingController _secs = TextEditingController();
  double xa = 0.0;
  double ya = 0.0;
  double d = 0.0;
  double deg = 0.0;
  double mins = 0.0;
  double secs = 0.0;
  String res = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 50),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _Xa,
                  onSubmitted: (value) {
                    setState(() {
                      xa = double.parse(_Xa.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Xa:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _Ya,
                  onSubmitted: (value) {
                    setState(() {
                      ya = double.parse(_Ya.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Ya:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _d,
                  onSubmitted: (value) {
                    setState(() {
                      d = double.parse(_d.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите горизонтальное проложение линии:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _degree,
                  onSubmitted: (value) {
                    setState(() {
                      deg = double.parse(_degree.text);
                      res = GeoMath.DirectTask(xa,ya,d,deg,mins,secs);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите градусы:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _mins,
                  onSubmitted: (value) {
                    setState(() {
                      mins = double.parse(_mins.text);
                      res = GeoMath.DirectTask(xa,ya,d,deg,mins,secs);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите минуты",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _secs,
                  onSubmitted: (value) {
                    setState(() {
                      secs = double.parse(_secs.text);
                      res = GeoMath.DirectTask(xa,ya,d,deg,mins,secs);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите секунды",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              SelectableText(
                res,
                style: TextStyle(fontSize: 25),
                onTap: () {
                  _Xa.clear();
                  Clipboard.setData(ClipboardData(text: res)).then(
                        (_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text(
                            "Текст скопирован!",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}