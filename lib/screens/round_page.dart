import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utils/geo_math.dart';

void main() => runApp(const RoundPage());

class RoundPage extends StatefulWidget {
  const RoundPage({super.key});

  @override
  State<RoundPage> createState() => _nameState();
}

class _nameState extends State<RoundPage> {
  final TextEditingController _xa = TextEditingController();
  final TextEditingController _ya = TextEditingController();
  final TextEditingController _xb = TextEditingController();
  final TextEditingController _yb = TextEditingController();
  double xa = 0.0;
  double ya = 0.0;
  double xb = 0.0;
  double yb = 0.0;
  String res = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 100),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _xa,
                  onSubmitted: (value) {
                    setState(() {
                      xa = double.parse(_xa.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Xa:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _ya,
                  onSubmitted: (value) {
                    setState(() {
                      ya = double.parse(_ya.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Ya:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _xb,
                  onSubmitted: (value) {
                    setState(() {
                      xb = double.parse(_xb.text);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Xb:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _yb,
                  onSubmitted: (value) {
                    setState(() {
                      yb = double.parse(_yb.text);
                      res = GeoMath.RoundTask(xa,ya,xb,yb);
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите Yb:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              SelectableText(
                res,
                style: TextStyle(fontSize: 25),
                onTap: () {
                  _xa.clear();
                  Clipboard.setData(ClipboardData(text: res)).then(
                        (_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text(
                            "Текст скопирован!",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}