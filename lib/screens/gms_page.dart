import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../utils/geo_math.dart';


void main() => runApp(const GMSPage());

class GMSPage extends StatefulWidget {
  const GMSPage({super.key});

  @override
  State<GMSPage> createState() => _nameState();
}

class _nameState extends State<GMSPage> {
  final TextEditingController _textcontroller1 = TextEditingController();
  final TextEditingController _textcontroller2 = TextEditingController();
  final TextEditingController _textcontroller3 = TextEditingController();
  double hours = 0.0;
  double mins = 0.0;
  double secs = 0.0;
  String res = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 150),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _textcontroller1,
                  onSubmitted: (value) {
                    setState(() {
                      hours = double.parse(_textcontroller1.text);
                      res = GeoMath.toDeg(hours, mins, secs).toString();
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите градусы:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _textcontroller2,
                  onSubmitted: (value) {
                    setState(() {
                      mins = double.parse(_textcontroller2.text);
                      res = GeoMath.toDeg(hours, mins, secs).toString();
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите минуты:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: TextField(
                  controller: _textcontroller3,
                  onSubmitted: (value) {
                    setState(() {
                      secs = double.parse(_textcontroller3.text);
                      res = GeoMath.toDeg(hours, mins, secs).toString();
                    });
                  },
                  decoration: const InputDecoration(
                      labelText: "Введите секунды:",
                      border: OutlineInputBorder()),
                  keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              SelectableText(
                res,
                style: TextStyle(fontSize: 25),
                onTap: () {
                  _textcontroller1.clear();
                  Clipboard.setData(ClipboardData(text: res)).then(
                        (_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text(
                            "Текст скопирован!",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}